<?php
use PHPUnit\Framework\TestCase;

error_reporting( E_ALL );
ini_set('error_reporting', E_ALL);

include_once 'api/_inc/config.inc.php';

require './vendor/autoload.php';
/*
    Test functions must start with "test"    
*/


    class appTest extends \PHPUnit_Framework_TestCase {

    	// records used for testing
    	private $invalidGt = "";
	    private $existingGt = "TerryCruiz";
    	private $validGt;

    	private $invalidLocale = "en-GB";
    	private $validLocale = "en-SG";


    	function __construct() {

    		/* Create a unique gamertag
    		   to be used for testing 
    		   purposes */

    		$this->validGt = uniqid('user');
    	}


	/* 

	    Endpoint Check Gamertag Entry 

	*/


	    /**
	     * Checks for null gamertags
	     */
	    public function testCheckNullGamertag() {
	    	$app = new appClass();

	    	$result = $app->checkGamertag();
	    	$this->assertEquals(false, $result);
	    }




	    /**
	     * Test for entry of empty strings
	     */
	    public function testCheckEmptyString() {
	    	$app = new appClass();

	    	$result = $app->checkGamertag('');
	    	$this->assertEquals(false, $result);
	    }




	    /**
	     * Tests for gamertag that isn't in database
	     * but is valid
	     */
	    public function testGamertagEmptyString() {
	    	$app = new appClass();

	    	$gamertag = "";
	    	$result = $app->checkGamertag($gamertag);

	    	$this->assertEquals(false, $result);
	    }




	    /**
	     * Gamertag doesn't have entry
	     * in database
	     */
	    public function testGamertagNotInDB() {
	    	$app = new appClass();

	    	$gamertag = "KANYEWEST6000";
	    	$result = $app->checkGamertag($gamertag);

	    	$this->assertEquals(false, $result);
	    }




	    /**
	     * Gamertag has an entry 
	     * already in the database
	     */
	    public function testGamertagInDB() {
	    	$app = new appClass();

	    	$gamertag = "Craig";
	    	$result = $app->checkGamertag($gamertag);

	    	$this->assertContains($result['gamertag'], 'craig');
	    }




	/*

	    Guzzle tests for CheckGamertag Endpoint

	 */




	    /**
	     * Ajax request to check invalid gamertag
	     * parameter to checkGamertag endpoint
	     */
	    function testInvalidGamertagInDBHttp(){
	    	$client = new GuzzleHttp\Client();
	    	$res = $client->request('GET', 'http://ffxapi.dev/checkGamertag?gamertag=');

	    	$res = json_decode($res->getBody());
	    	$this->assertEquals('empty', $res->status);
	    	$this->assertEquals(array(), $res->errors);
	    }




	    /**
	     * Ajax request to check gamertag
	     * that is already in database
	     */
	    function testGamertagInDBHttp(){
	    	$client = new GuzzleHttp\Client();
	    	$res = $client->request('GET', 'http://ffxapi.dev/checkGamertag?gamertag=craig');
	    	$res = json_decode($res->getBody());

	    	$this->assertEquals('found', $res->status);
	    	$this->assertEquals(array(), $res->errors);
	    }




	    /**
	    * Ajax request to check gamertag
	    * that isn't in the database
	    */
	    function testGamertagNotInDBHttp(){
	    	$client = new GuzzleHttp\Client();
	    	$res = $client->request('GET', 'http://ffxapi.dev/checkGamertag?gamertag=kanyewest99000');
	    	$res = json_decode($res->getBody());

	    	$this->assertEquals('empty', $res->status);
	    	$this->assertEquals(array(), $res->errors);
	    }





	/*

	    Add Gamertag Endpoint

	 */




	    /**
	     * Try to add invalid locale to 
	     * gamertag table
	     */
	    function testAddUserInvalidLocaleValidGamertag() {
	    	$app = new appClass();

	    	$gamertag = $this->validGt;
	        // en-gb is an invalid locale
	    	$locale = $this->invalidLocale;

	    	$result = $app->addGamertag($gamertag, $locale);

	    	$this->assertEquals("error", $result["status"]);
	    	$this->assertEquals(601, $result["code"]);
	    }



	    /**
	     * 	Try an add an entry without a
	     * 	locale defined
	     */
	    function testAddUserEmptyLocale() {
	    	$app = new appClass();

	    	$gamertag = $this->validGt;
	        // en-gb is an invalid locale
	    	$locale = "";

	    	$result = $app->addGamertag($gamertag, $locale);

	    	$this->assertEquals("error", $result["status"]);
	    	$this->assertEquals(601, $result["code"]);
	    }




	    /**
	     * Add users with valid locale but
	     * invalid gamertag
	     */
	    function testAddUserValidLocaleEmptyGamertag() {
	    	$app = new appClass();

	    	$gamertag = '';
	    	$locale = $this->validLocale;

	    	$result = $app->addGamertag($gamertag, $locale);

	    	$this->assertEquals("error", $result["status"]);
	    	$this->assertEquals(602, $result["code"]);
	    }




	    /**
	     * Insert a valid gamertag && locale
	     * into the database
	     */
	    function testAddValidGamertag() {
	    	$app = new appClass();

	    	$gamertag = $this->validGt;
	    	$locale = $this->validLocale;

	    	$result = $app->addGamertag($gamertag, $locale);
	    	$this->assertEquals("success", $result["status"]);
	    }




	    /**
	     * Add gamertag that is already in database
	     */
	    function testAddDuplicateGamertag() {
	    	$app = new appClass();

	    	// because validGt has already
	    	// been used in test
	    	$gamertag = $this->existingGt;
	    	$locale = $this->validLocale;

	    	$result = $app->addGamertag($gamertag, $locale);
	    	$this->assertEquals("success", $result["status"]);
	    	$this->assertEquals("101", $result["code"]);
	    }
	}
?>