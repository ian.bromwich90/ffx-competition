<?php
/**
 * test
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/test', function (Request $request, Response $response, $args) {

    $data = array('name' => 'Bob', 'age' => 40);
    
    return $response->withJson($data);
});
?>