<?php
/**
 * checkGamertag for entry status
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/checkGamertag', function (Request $request, Response $response, $args) {

	// default set of errors
	$resp = [
		'status' => 'empty',
		'errors' => []
	];

	// get parameters from
	// query string
	$gamertag = $request->getParam('gamertag');
    
    // create instance of app class
    $appClass = new appClass();

	if($dbgamertag = $appClass->checkGamertag($gamertag)) {
		$resp['status'] = "found";
	}	

    return $response->withJson($resp);
});
?>