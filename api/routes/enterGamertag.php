<?php
/**
 * submit gamertag into database
 * for preorder bonus
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->post('/enterGamertag', function (Request $request, Response $response, $args) {

	// default set of errors
	$resp = [
		'status' => 'error',
		'errors' => []
	];

	// get parameters from
	// query string
	$gamertag = $request->getParam('gamertag');
	$locale = $request->getParam('locale'); 

    // create instance of app class
    // and check response from gt check
    $appClass = new appClass();

    // call add gamertag method 
    // gamertag and locale as paramters
    $databaseEntry = $appClass->addGamertag($gamertag, $locale);

    // add gamertag to database
    // update response to success
    // or update error messages
	if($databaseEntry["status"] === "success") {

		$resp['status'] = "success";
	} else {

		// push errors to response
		$resp['errors'][] = "Gamertag couldn't be added";
		$resp['errors'][] = $databaseEntry['code'];
	}

    return $response->withJson($resp);
});
?>