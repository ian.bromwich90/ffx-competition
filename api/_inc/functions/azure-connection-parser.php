<?php
/* connStrToArray 
------------------
converts connection string into 
an array 
*/
function connStrToArray($connStr){
	$connArray = array();
	$stringParts = explode(";", $connStr);
	foreach($stringParts as $part){
		$temp = explode("=", $part);
		$connArray[$temp[0]] = $temp[1];
	}
	return $connArray;
}

?>