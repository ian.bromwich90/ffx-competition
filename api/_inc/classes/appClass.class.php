<?php

class appClass {
    
	private $connection, $name;
	private $tableName = "gamertags";

    /*  Array of locales that are
        available to enter */
    private $locales = array('en-sg', 'en-hk', 'zh-hk', 'zh-tw', 'ko-kr');


    /* CheckGamertag 
    ------------------------
    Checks database to see if gamertag
    is already in database
    +return array with gamertag info or false
    */
    public function checkGamertag($gamertag = false) {
    	$rowFound = false;

    	// check if gamertag has been set
    	if(!empty($gamertag)) {
    		$rowFound = $this->selectRecord($gamertag);
    	}
    	return $rowFound;
    }



    /* Add Gamertag
    -------------------------
    Adds user to database
    using parameter values 
    +return boolean based on success of addition to database
    +return array with error details
    */
    public function addGamertag($gamertag = null, $locale = null) {
        // create default response array
        $resp = array(
            "status" => "error",
            "code" => 600,
            "message" => ""
        );

        // update locale to lowercase
        // for matching against valid
        $locale = strtolower($locale);

        // validate entered variables
        if(empty($gamertag)) {

            $resp["code"] = 602;
            $resp["message"] = "Invalid Gamertag";

            return $resp;

        } elseif(is_null($locale) || !in_array($locale, $this->locales)) {

            $resp["code"] = 601;
            $resp["message"]  = "Invalid Locale";

            return $resp;

        }

    	// if validation is passed then
    	// insert record into database
    	$result = $this->insertRecord($gamertag, $locale);

        if($result) {
            $resp["status"] = "success";
            $resp["code"] = "101";

            return $resp;
        } else {
            $resp["code"] = "600";
            $resp["message"] = "Database error";

            return $resp;
        }
    	return $resp;
    }


    /* Select record
    -------------------------
    Attempt to find a single
    record return status of
    action
    + return
        - status (boolean)
    */
    private function selectRecord($gamertag = null) {
    	$dbEntry = false;

    	if($gamertag) {
	        // Structure query and try to insert
	        // row ito tracking table
	        $db = $this->getConnection()->getInstance();

	        // query db for gamertag
	    	$db->where('gamertag', $gamertag);
	    	$result = $db->getOne($this->tableName);

	    	// if result is not found 
	    	if(!empty($result)) {
	    		$dbEntry = $result;
	    	}
    	}

    	return $dbEntry;
    }


    /* Insert Record
    ---------------------
    Insert a record into the 
    database with input params
    */
    private function insertRecord($gamertag = null, $locale = null) {

        // get instance of open 
        // connection
    	$db = $this->getConnection()->getInstance();


    	// create array of data to enter
    	// into database
    	$data = Array(
    		'gamertag' => $gamertag,
    		'created_at' => date('Y-m-d H:i:s'),
    		'locale' => $locale
    	);

        // check if database returns 
        // error on duplicate gamertag entry
    	$insert = $db->insert($this->tableName, $data);

        // if insert errors find out why ?
        if(!$insert) {

            // check gamertag for entries
            // into current database
            if($this->checkGamertag($gamertag)) {

                // return true as entry
                // has been recorded previously
                $insert = true;
            }
        }

    	return $insert ?: false;
    }


    /* GetConnection
    ------------------
    Returns open db connection if available, 
    else opens new connection and sets as 
    class property
    */
    private function getConnection() {

        // if connection isn't defined open
        // db connection and assign to property
        if(!$this->connection) {
             $this->connection = new MysqliDb (DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        }

        return $this->connection;
    }
    
} 