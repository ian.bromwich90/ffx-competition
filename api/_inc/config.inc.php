<?php
/**********************************************
*
* GENERAL DEFINTITONS 
*
**********************************************/
DEFINE('DS','/');
DEFINE('INC_ROOT',dirname(__FILE__).DS);  
DEFINE('CLASSES_ROOT', INC_ROOT.'classes'.DS);
DEFINE('FUNCTIONS_ROOT', INC_ROOT.'functions'.DS);
DEFINE('ROUTES_ROOT', str_replace('_inc'.DS, 'routes'.DS,INC_ROOT));


/*
 * Set default timezone for server
 */
date_default_timezone_set('Europe/London');

//For db documentation see https://github.com/joshcam/PHP-MySQLi-Database-Class

/**********************************************
*
* CLASSESS
* App specific custom classes not loaded via 
* composer
*
**********************************************/

require CLASSES_ROOT.'appClass.class.php';



/**********************************************
*
* FUNCTIONS
* Custom functions required by the app outside
* of scope of the app class
*
**********************************************/
    
 require FUNCTIONS_ROOT.'azure-connection-parser.php';   
 


/**********************************************
*
* ENVIROMENTAL DEFINITONS
* Basic - added to cases as required
*
**********************************************/
 
 /*
     For running via command line (Unit tests)
     HTTP_HOST doesn't exist - so set it to localhost
 
 */
 if(!isset($_SERVER['HTTP_HOST'])) {
     $_SERVER['HTTP_HOST'] = 'ffxapi.dev';
 }


switch($_SERVER['HTTP_HOST']){

	case 'ffxapi.dev':

		DEFINE('DB_HOST', '127.0.0.1');
		DEFINE('DB_USER', 'root');
		DEFINE('DB_PASSWORD', 'root');
		DEFINE('DB_DATABASE', 'ff_api');

		DEFINE('ENVIRONMENT', 'DEVELOPMENT');



		/**********************************************
		*
		* ERROR REPORTING
		*
		**********************************************/

		ini_set('display_errors', 1);
		error_reporting(E_ALL|E_STRICT);

	break;

	case 'heroku': 
	
        //Heroku stores DB creds in ENV variable
        $url=parse_url(getenv("CLEARDB_DATABASE_URL"));
	   
    	// Define database variables 
		DEFINE('DB_HOST', $url["host"]);
		DEFINE('DB_USER', $url["user"]);
		DEFINE('DB_PASSWORD', $url["pass"]);
		DEFINE('DB_DATABASE', substr($url["path"],1));
	
	
	   	DEFINE('ENVIRONMENT', 'PRODUCTION'); 
		
	
		/**********************************************
		*
		* ERROR REPORTING - OFF
		*
		**********************************************/
	
	   	ini_set('display_errors', 0);
	    error_reporting(0);
	
	break;
    
    
	case 'ffx-preorder.azurewebsites.net': 
	
    	$dbConn = connStrToArray(getenv("MYSQLCONNSTR_defaultConnection"));

    	// Define database variables 
    	DEFINE('DB_DATABASE', $dbConn["Database"]);
    	DEFINE('DB_USER', $dbConn["User Id"]);
    	DEFINE('DB_PASSWORD', $dbConn["Password"]);
    	DEFINE('DB_HOST', $dbConn["Data Source"]);
	    
	   	DEFINE('ENVIRONMENT', 'PRODUCTION'); 
        
	
		/**********************************************
		*
		* ERROR REPORTING - OFF
		*
		**********************************************/
	
	   	ini_set('display_errors', 0);
	    error_reporting(0);
	
	break; 
     
	
	default: 
		die("No config setup found for ".$_SERVER['HTTP_HOST']);
	break;
}



    
    
?>