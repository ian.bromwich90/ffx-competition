<?php
header("Access-Control-Allow-Origin: *");

 
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include '_inc/config.inc.php';

require '../vendor/autoload.php';

/**********************************************
*
* Slim display errors
*
**********************************************/
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

/**********************************************
*
* Inisitate new Slim Obj
*
**********************************************/
$app = new \Slim\App(["settings" => $config]);


// set container
$container = $app->getContainer();
$settings  = $container->get('settings');

// log errors
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

/**********************************************
*
* Slim error handling
*
**********************************************/
//If in production - set in config.inc.php
if(ENVIRONMENT == 'PRODUCTION'):
// custom 'not allowed' error
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-type', 'text/html')
            ->write('');
    };
};

// custom 'not found' error
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('');
    };
};
endif;


/**********************************************
*
* Include routes
*
**********************************************/
$routeFiles = (array) glob(ROUTES_ROOT.'*.php');
foreach($routeFiles as $routeFile) {
    require $routeFile;
}


/**********************************************
*
* Run app
*
**********************************************/
$app->run();


?>